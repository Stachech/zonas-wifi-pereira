<!DOCTYPE html>
<html> 
<head> 
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" /> 
  <title>ZONAS WIFI PEREIRAs</title> 
  <script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>
</head>

<?php 
      $final="";
      foreach ($coordenadas as $datos) 
      {

        $inicial="['".$datos->id_coordenada."',". $datos->latitud.",".$datos->longitud."],";
        $final .= $inicial;
  
    }

   
    ?> 
<body>
  <div id="map" style="width: 100%; height: 1000px;"></div>

  <script type="text/javascript">
    var locations = [<?php echo  $final; ?>];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 12,
      center: new google.maps.LatLng(4.81321, -75.6946),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>
</body>
</html>


