
<?php

class GuardarCoordenadas extends CI_Model 
{

	public function __construct(){
        //
        parent::__construct();
    }  
	

      public function guardar ()
		{
			$this->load->database();
			
			$json = file_get_contents('https://www.datos.gov.co/resource/ffwn-j5cw.json?$$app_token=N7RLE82zWsTBav7Ju9sVIHAjK&$select=ubicaci_n');
			$datoscoordenadas = json_decode($json, true);


			foreach ($datoscoordenadas as $coordenadas) 
			{
				$pos = explode(",",$coordenadas['ubicaci_n']);

				$latitud = str_replace('(', "",$pos[0]);
				$longitud = str_replace(')', "",$pos[1]);

				$data = array('latitud' => $latitud,'longitud' => $longitud);

				$this->db->insert('coordenadas', $data);
			        
			        
			}

		}

		public function consultar ()
		{
			$this->load->database();
			
			$query = $this->db->get('coordenadas'); 

			return $query->result(); ;

		}

		public function verificador()
		{
			$this->load->database();
			
			$query = $this->db->get('coordenadas');

			$num_rows = $this->db->count_all_results('coordenadas');

			return $num_rows;

		}


}

?>

